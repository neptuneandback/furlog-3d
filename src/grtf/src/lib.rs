use std::string::String;
use std::vec::Vec;
use std::boxed::Box;

/// This trait is used throughout the GRTF for testing equivalence
/// and to gather human readable information about the object being tested.
pub trait Testable {
    /// Returns true if self equals other, false otherwise
    fn equals(&self, _other:&Self) -> bool;
    /// Returns a string representation of this object. This will often be called to
    /// provide contextual information into an assertion result
    fn to_string(&self) -> String;
}

pub trait Test {
    /// This method is run to set up the test.
    /// This method should be used to allocate necessary memory, etc,
    /// and prepare the test to run.
    /// This method will also be run before any tests are executed.
    fn init(&self);

    /// This method will actually run the tests.
    /// An Assert instance is provided to do assertions.
    fn run_test(&self, _assert:&Assert);

    /// This method will run after all tests have been executed.
    fn post_test(&self);
}

pub trait Reporter {
    fn log(&self, _rr:AssertReport);
}

struct EmptyReporter {

}

impl Reporter for EmptyReporter {
    fn log(&self, _rr:AssertReport) {}
}

pub struct TestsRunner {
    pub tests: Vec<Box<Test>>
}

impl TestsRunner {
    pub fn run_tests(&self, reporter:std::option::Option<Box<Reporter>>) {
        for t in &self.tests {
            t.init();
        }

        let assert = Assert {
            reporter: match reporter {
                Some(x)=>x,
                None=>Box::new(EmptyReporter{})
            }
        };

        for t in &self.tests {
            t.run_test(&assert);
        }

        for t in &self.tests {
            t.post_test();
        }
    }
}

/// A AssertReport contains contextual information (like time) as well as information
/// about an assertion that failed or succeeded.
/// This struct can also be used to store error codes or other miscellaneous information.
pub struct AssertReport {
    /// Result of the assertion. True if the assertion resulted in true, false otherwise.
    assertion_result:bool,

    /// A short message. Used to describe the details of the report.
    msg: String,

    /// Vec of tuples used for miscellaneous details and can store contextual information
    /// for further processing.
    context: Vec<(String, String)>
}

impl AssertReport {
    /// Returns a new AssertReport instance
    pub fn new(_assertion_result:bool, _msg:String, _context:Vec<(String, String)>) -> AssertReport {
        return AssertReport {assertion_result:_assertion_result, msg:_msg, context:_context};
    }
}

/// This struct is used to make assertions. Some functions can use
/// the internal `Reporter` instance in the `Assert` instance.
pub struct Assert {
    reporter: Box<Reporter>
}

impl Assert {
    /// Asserts that to Testable objects are equalivalent by using their .equals()
    /// methods. This method will not log any information, but will print to
    /// stderr in the event of an assertion fail.
    pub fn assert_equals<T:Testable>(&self, _t1:&T, _t2:&T) {
        if !_t1.equals(_t2) {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1.to_string(), _t2.to_string());
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an i8 rather than
    /// a Testable.
    pub fn assert_equals_i8(&self, _t1:i8, _t2:i8) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an i16 rather than
    /// a Testable.
    pub fn assert_equals_i16(&self, _t1:i16, _t2:i16) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an i32 rather than
    /// a Testable.
    pub fn assert_equals_i32(&self, _t1:i32, _t2:i32) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an i64 rather than
    /// a Testable.
    pub fn assert_equals_i64(&self, _t1:i64, _t2:i64) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an u8 rather than
    /// a Testable.
    pub fn assert_equals_u8(&self, _t1:u8, _t2:u8) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an u16 rather than
    /// a Testable.
    pub fn assert_equals_u16(&self, _t1:u16, _t2:u16) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an u32 rather than
    /// a Testable.
    pub fn assert_equals_u32(&self, _t1:u32, _t2:u32) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an u64 rather than
    /// a Testable.
    pub fn assert_equals_u64(&self, _t1:u64, _t2:u64) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an isize rather than
    /// a Testable.
    pub fn assert_equals_isize(&self, _t1:isize, _t2:isize) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an usize rather than
    /// a Testable.
    pub fn assert_equals_usize(&self, _t1:usize, _t2:usize) {
        if _t1 != _t2 {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn assert_equals_f32(&self, _t1:f32, _t2:f32, _epsilon:f32) {
        if (_t1 - _t2).abs() >= _epsilon {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn assert_equals_f64(&self, _t1:f64, _t2:f64, _epsilon:f64) {
        if (_t1 - _t2).abs() >= _epsilon {
            eprintln!("ASSERTION FAILED. {} is not equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an i8 rather than
    /// a Testable.
    pub fn rep_assert_equals_i8(&self, _t1:i8, _t2:i8) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i8"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an i16 rather than
    /// a Testable.
    pub fn rep_assert_equals_i16(&self, _t1:i16, _t2:i16) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i16"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an i32 rather than
    /// a Testable.
    pub fn rep_assert_equals_i32(&self, _t1:i32, _t2:i32) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i32"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an i64 rather than
    /// a Testable.
    pub fn rep_assert_equals_i64(&self, _t1:i64, _t2:i64) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i64"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an u8 rather than
    /// a Testable.
    pub fn rep_assert_equals_u8(&self, _t1:u8, _t2:u8) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u8"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an u16 rather than
    /// a Testable.
    pub fn rep_assert_equals_u16(&self, _t1:u16, _t2:u16) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u16"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an u32 rather than
    /// a Testable.
    pub fn rep_assert_equals_u32(&self, _t1:u32, _t2:u32) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u32"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an u64 rather than
    /// a Testable.
    pub fn rep_assert_equals_u64(&self, _t1:u64, _t2:u64) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u64"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an isize rather than
    /// a Testable.
    pub fn rep_assert_equals_isize(&self, _t1:isize, _t2:isize) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("isize"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an usize rather than
    /// a Testable.
    pub fn rep_assert_equals_usize(&self, _t1:usize, _t2:usize) {
        self.reporter.log(
            AssertReport::new(
                _t1 == _t2,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("usize"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn rep_assert_equals_f32(&self, _t1:f32, _t2:f32, _epsilon:f32) {
        self.reporter.log(
            AssertReport::new(
                (_t1 - _t2).abs() < _epsilon,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("_epsilon"), format!("{}", _epsilon)),
                    (String::from("type"), String::from("f32"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn rep_assert_equals_f64(&self, _t1:f64, _t2:f64, _epsilon:f64) {
        self.reporter.log(
            AssertReport::new(
                (_t1 - _t2).abs() < _epsilon,
                String::from("Assert equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("_epsilon"), format!("{}", _epsilon)),
                    (String::from("type"), String::from("f64"))
                ]
            )
        );
    }

    /// This method is similar to assert_equals(), however,
    /// this method has additional functionality: a Reporter will be used.
    /// Whether the assertion fails or succeeds, the internal Reporter will be
    /// notified of the assertion result.
    pub fn rep_assert_equals<T:Testable>(&self, _t1:&T, _t2:&T) {
        self.reporter.log(
            AssertReport::new(
                _t1.equals(_t2),
                String::from("Assert equals"),
                vec![(String::from("t1"), _t1.to_string()), (String::from("t2"), _t2.to_string())]
            )
        );
    }

    /// ---------- Assert not equals methods

    /// Asserts that to Testable objects are NOT equalivalent by using their .equals()
    /// methods. This method will not log any information, but will print to
    /// stderr in the event of an assertion fail.
    pub fn assert_not_equals<T:Testable>(&self, _t1:&T, _t2:&T) {
        if _t1.equals(_t2) {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1.to_string(), _t2.to_string());
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an i8 rather than
    /// a Testable.
    pub fn assert_not_equals_i8(&self, _t1:i8, _t2:i8) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an i16 rather than
    /// a Testable.
    pub fn assert_not_equals_i16(&self, _t1:i16, _t2:i16) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an i32 rather than
    /// a Testable.
    pub fn assert_not_equals_i32(&self, _t1:i32, _t2:i32) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an i64 rather than
    /// a Testable.
    pub fn assert_not_equals_i64(&self, _t1:i64, _t2:i64) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an u8 rather than
    /// a Testable.
    pub fn assert_not_equals_u8(&self, _t1:u8, _t2:u8) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an u16 rather than
    /// a Testable.
    pub fn assert_not_equals_u16(&self, _t1:u16, _t2:u16) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an u32 rather than
    /// a Testable.
    pub fn assert_not_equals_u32(&self, _t1:u32, _t2:u32) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an u64 rather than
    /// a Testable.
    pub fn assert_not_equals_u64(&self, _t1:u64, _t2:u64) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an isize rather than
    /// a Testable.
    pub fn assert_not_equals_isize(&self, _t1:isize, _t2:isize) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an usize rather than
    /// a Testable.
    pub fn assert_not_equals_usize(&self, _t1:usize, _t2:usize) {
        if _t1 == _t2 {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn assert_not_equals_f32(&self, _t1:f32, _t2:f32, _epsilon:f32) {
        if (_t1 - _t2).abs() < _epsilon {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as assert_not_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn assert_not_equals_f64(&self, _t1:f64, _t2:f64, _epsilon:f64) {
        if (_t1 - _t2).abs() < _epsilon {
            eprintln!("ASSERTION FAILED. {} is equal to {}", _t1, _t2);
        }
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an i8 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_i8(&self, _t1:i8, _t2:i8) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i8"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an i16 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_i16(&self, _t1:i16, _t2:i16) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i16"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an i32 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_i32(&self, _t1:i32, _t2:i32) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i32"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an i64 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_i64(&self, _t1:i64, _t2:i64) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("i64"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an u8 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_u8(&self, _t1:u8, _t2:u8) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u8"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an u16 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_u16(&self, _t1:u16, _t2:u16) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u16"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an u32 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_u32(&self, _t1:u32, _t2:u32) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u32"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an u64 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_u64(&self, _t1:u64, _t2:u64) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("u64"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an isize rather than
    /// a Testable.
    pub fn rep_assert_not_equals_isize(&self, _t1:isize, _t2:isize) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("isize"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an usize rather than
    /// a Testable.
    pub fn rep_assert_not_equals_usize(&self, _t1:usize, _t2:usize) {
        self.reporter.log(
            AssertReport::new(
                _t1 != _t2,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("type"), String::from("usize"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_f32(&self, _t1:f32, _t2:f32, _epsilon:f32) {
        self.reporter.log(
            AssertReport::new(
                (_t1 - _t2).abs() >= _epsilon,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("_epsilon"), format!("{}", _epsilon)),
                    (String::from("type"), String::from("f32"))
                ]
            )
        );
    }

    /// Same functionality as rep_assert_not_equals(Testable, Testable), except takes in an f32 rather than
    /// a Testable.
    pub fn rep_assert_not_equals_f64(&self, _t1:f64, _t2:f64, _epsilon:f64) {
        self.reporter.log(
            AssertReport::new(
                (_t1 - _t2).abs() >= _epsilon,
                String::from("Assert not equals"),
                vec![
                    (String::from("t1"), _t1.to_string()),
                    (String::from("t2"), _t2.to_string()),
                    (String::from("_epsilon"), format!("{}", _epsilon)),
                    (String::from("type"), String::from("f64"))
                ]
            )
        );
    }

    /// This method is similar to assert_not_equals(), however,
    /// this method has additional functionality: a Reporter will be used.
    /// Whether the assertion fails or succeeds, the internal Reporter will be
    /// notified of the assertion result.
    pub fn rep_assert_not_equals<T:Testable>(&self, _t1:&T, _t2:&T) {
        self.reporter.log(
            AssertReport::new(
                !_t1.equals(_t2),
                String::from("Assert not equals"),
                vec![(String::from("t1"), _t1.to_string()), (String::from("t2"), _t2.to_string())]
            )
        );
    }

    /// --- Misc methods

    /// This method will force report a failure to the Reporter instance.
    pub fn rep_fail(&self, _msg:String, context:Vec<(String, String)>) {
        self.reporter.log(
            AssertReport::new(
                false,
                _msg,
                context
            )
        );
    }

    /// This method will force report a success to the Reporter instance.
    pub fn rep_success(&self, _msg:String, context:Vec<(String, String)>) {
        self.reporter.log(
            AssertReport::new(
                true,
                _msg,
                context
            )
        );
    }
}

impl Testable for String {
    fn equals(&self, _other:&Self) -> bool {
        self.eq(_other)
    }

    fn to_string(&self) -> String {
        self.clone()
    }
}
