extern crate grtf;

use self::grtf::{Test, Assert};

extern crate locale;
use self::locale::LocaleCSVKey;

pub struct LocaleTests {}

impl Test for LocaleTests {
    fn init(&self) {

    }

    fn run_test(&self, _assert:&Assert) {
        _assert.assert_equals(&locale::US, &locale::US);

        println!("{}", locale::US.get_displayable_property(LocaleCSVKey::DisplayName));
        println!("{}", locale::US.get_displayable_property(LocaleCSVKey::DisplayCountry));
        println!("{}", locale::US.get_converted_displayable_property(LocaleCSVKey::DisplayCountry, &locale::SPAIN));
    }

    fn post_test(&self) {

    }
}
