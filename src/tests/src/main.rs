extern crate grtf;

use std::boxed::Box;
use grtf::TestsRunner;

extern crate tests;
use tests::locale_tests::LocaleTests;
use tests::math_tests::MathTests;

fn main() {
    let runner = TestsRunner {
        tests: vec![Box::new(LocaleTests{}), Box::new(MathTests{})]
    };

    runner.run_tests(None);
}
