extern crate std;
use std::string::String;
use std::fs::File;
use std::io::Read;

extern crate grtf;

extern crate json;
use self:: {json::JsonValue};

pub struct Locale {
    /// The country/region code for this locale, which should either be the empty string,
    /// an uppercase ISO 3166 2-letter code, or a UN M.49 3-digit code.
    pub country: &'static str,

    /// A path to a csv file that holds the following information:
    /// * `display_name`
    ///     * A name for the locale that is displayable to the user
    /// * `display_lang`
    ///     * A name for the locale's language that is displayable to the user
    /// * `display_country`
    ///     * A name for the locale's country that is displayable to the user
    /// * `convert`
    ///     * A JSON object that has multiple other country/region codes as the key and each value
    ///       has `display_name`, `display_lang`, `display_country` in that country/region's language.
    /// ```json
    /// {
    /// "display_name": "English (United States)",
    /// "display_lang": "English",
    /// "display_country": "United States",
    /// "convert": {
    ///     "ES": { //Spain
    ///         "display_name": "Inglés (Estados Unidos)",
    ///         "display_lang": "Inglés",
    ///         "display_country": "Estados Unidos"
    ///     }
    /// }
    /// }
    /// ```
    properties_json_file_path: &'static str
}

static MISCONF:&'static str = "locale json misconfiguration";

impl Locale {
    /// Gets a displayable value for a user to read. Pass in the key (property) to retrieve.
    /// The returned value will be in the locale's language itself.
    pub fn get_displayable_property(&self, _key:LocaleCSVKey) -> String {
        let mut json_txt = String::new();
        let mut file = File::open(self.properties_json_file_path).unwrap();
        file.read_to_string(&mut json_txt).unwrap();

        let json = json::parse(json_txt.as_str()).unwrap();

        match json {
            JsonValue::Object(o) => match o.get(_key.key()).unwrap() {
                x => x.as_str().unwrap().repeat(1) //create a clone of the String
            },
            _ => panic!(MISCONF)
        }

    }

    /// Gets a displayable value for a user to read. Pass in the key (property) to retrieve.
    /// The returned value will NOT be in the locale's language itself.
    /// Instead, it will be translated into the provided Locale
    pub fn get_converted_displayable_property(&self, _key:LocaleCSVKey, _l:&Locale) -> String {
        let mut json_txt = String::new();
        let mut file = File::open(self.properties_json_file_path).unwrap();
        file.read_to_string(&mut json_txt).unwrap();

        let json = json::parse(json_txt.as_str()).unwrap();

        let convert_obj = match json {
            JsonValue::Object(ref o) => o.get("convert").unwrap(),
            _ => panic!(MISCONF)
        };

        let lang_obj = match convert_obj {
            &JsonValue::Object(ref o) => o.get(_l.country).unwrap(),
            _ => panic!(MISCONF)
        };

        match lang_obj {
            &JsonValue::Object(ref o) => match o.get(_key.key()).unwrap() {
                x => x.as_str().unwrap().repeat(1) //create a clone of the String
            },
            _ => panic!(MISCONF)
        }

    }
}

pub enum LocaleCSVKey {
    DisplayName,
    DisplayLanguage,
    DisplayCountry
}

impl LocaleCSVKey {
    fn key(&self) -> &'static str {
        match *self {
            LocaleCSVKey::DisplayName => "display_name",
            LocaleCSVKey::DisplayLanguage => "display_lang",
            LocaleCSVKey::DisplayCountry => "display_country"
        }
    }
}

impl std::fmt::Display for Locale {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "locale::Locale{{country:{}}}", self.country)
    }
}

pub static US: Locale = Locale { country: "US", properties_json_file_path: "./furlog/locale.us.json"};
pub static SPAIN: Locale = Locale { country: "ES", properties_json_file_path: "./furlog/locale.es.json"};


// GRTF stuff
impl grtf::Testable for Locale {
    fn equals(&self, _other:&Self) -> bool {
        self.country.eq(_other.country)
    }

    fn to_string(&self) -> String {
        format!("{}", self)
    }
}
