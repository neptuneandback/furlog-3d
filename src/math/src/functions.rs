pub fn normalize_i64(value: i64, bottom: i64, top: i64) -> i64 {
    let width = top - bottom;
    let offset = value - bottom;

    offset - ((offset / width) * width) + bottom
}

pub fn normalize_f64(value: f64, bottom: f64, top: f64) -> f64 {
    let width = top - bottom;
    let offset = value - bottom;

    offset - ((offset / width).floor() * width) + bottom
}
