extern crate std;

use angle::Angle;

static NUM_DIGITS_TO_ROUND_TO: i32 = 2;

#[derive(Copy, Clone)]
pub struct Vector2{
  _x: f64,
  _y: f64
}

impl Vector2 {

    pub fn new_i32(x: i32, y: i32) -> Vector2 {
        Vector2::new_f64(x as f64, y as f64)
    }

    pub fn new_i64(x: i64, y: i64) -> Vector2 {
        Vector2::new_f64(x as f64, y as f64)
    }

    pub fn new_f32(x: f32, y: f32) -> Vector2 {
        Vector2::new_f64(x as f64, y as f64)
    }

    pub fn new_f64(x: f64, y: f64) -> Vector2 {
        Vector2 {
            _x: x,
            _y: y
        }
    }

    pub fn origin() -> Vector2 {
        Vector2::new_f64(0.0, 0.0)
    }

    pub fn distance_to(&self, other: Vector2) -> f64 {
        (self.x() - other.x()).hypot(self.y() - other.y())
    }

    pub fn move_by(&mut self, other: Vector2) {
        self._x += other.x();
        self._y += other.y();
    }

    pub fn x(&self) -> f64 {
        self._x
    }

    pub fn y(&self) -> f64 {
        self._y
    }

    //Rotates the Point around the origin
    pub fn rotate(&mut self, a: Angle) {
        let theta = a.normalize().as_radians().as_f64();

        let x_prime = (self.x() * theta.cos()) - (self.y() * theta.sin());
        let y_prime = (self.x() * theta.sin()) + (self.y() * theta.cos());

        self._x = x_prime;
        self._y = y_prime;
    }

}

impl std::fmt::Display for Vector2 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let round_factor =  10.0_f64.powi(NUM_DIGITS_TO_ROUND_TO);
        let round_x = (self.x() * round_factor).round() / round_factor;
        let round_y = (self.y() * round_factor).round() / round_factor;
        write!(f, "math::Vector2{{x:{}, y:{}}}", round_x, round_y)
    }
}

impl std::ops::Neg for Vector2 {
    type Output = Vector2;

    fn neg(self) -> Vector2 {
        Vector2::new_f64(-self.x(), -self.y())
    }
}
