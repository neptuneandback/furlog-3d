extern crate math;

use math::Angle;
use math::Triangle2;
use math::Vector2;

fn main() {
	let v = Vector2::new_i32(1, 1);
	let v2 = Vector2::new_i32(1, 2);
	let v3 = Vector2::new_i32(2, 1);

	let p = Vector2::new_i32(2, 2);

	let a = Angle::Degrees(90.0);

	let mut t = Triangle2::new(v, v2, v3);
	println!("{}", t);
	t.rotate_origin(a);
	println!("{}", t)
	//println!("4PI normalized is {}", Angle::normalize(b));

}
