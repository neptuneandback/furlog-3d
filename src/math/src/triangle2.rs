extern crate std;

use vector2::Vector2;
use angle::Angle;

pub struct Triangle2 {
	_a: Vector2,
	_b: Vector2,
	_c: Vector2,
}

impl Triangle2 {
	pub fn new(_a: Vector2, _b: Vector2, _c: Vector2) -> Triangle2{
		Triangle2 {
			_a,
			_b,
			_c
		}
	}

	pub fn translate_by_i64(&mut self, x: i64, y: i64) {
		self.translate(&Vector2::new_i64(x, y));
	}

	pub fn translate_by_f64(&mut self, x: f64, y: f64) {
		self.translate(&Vector2::new_f64(x, y));
	}

	pub fn translate_by_vector(&mut self, p: Vector2) {
		self.translate(&p);
	}

	fn translate(&mut self, translate_vector: &Vector2) {
		self._a.move_by(*translate_vector);
		self._b.move_by(*translate_vector);
		self._c.move_by(*translate_vector);
	}

	pub fn area(&self) -> f64 {
		0.0
	}

	pub fn scale_i64(&mut self, s: i64) {
		self.scale(s as f64);
	}

	pub fn scale_f64(&mut self, s: f64) {
		self.scale(s as f64);
	}

	fn scale(&self, s: f64) {

	}

	//Rotates the Triangle around itdself
	pub fn rotate_self(&mut self, a: Angle) {
		let center = self.midpoint();

		//Make the point of rotation (the midpoint) the origin
		self.translate_by_vector(-center);

		//Do the rotation
		self.rotate_origin(a);

		//Translate triangle back to original position
		self.translate_by_vector(center);
	}

	//Rotate the triangle around the origin
	pub fn rotate_origin(&mut self, a: Angle) {;
		self._a.rotate(a);
		self._b.rotate(a);
		self._c.rotate(a);


	}

	//Rotate the triangle around a point on the 2D plane
	//A positive angle will rotate counterclockwise
	pub fn rotate_point(&mut self, point: Vector2, a: Angle) {
		//Make the point of rotation the origin
		self.translate_by_vector(-point);

		//Do the rotation
		self.rotate_origin(a);

		//Translate triangle back to original position
		self.translate_by_vector(point);
	}

	// Fing the center of the triangle using averaged coordinates
	fn midpoint(&self) -> Vector2 {
		let x_avg = (self._a.x() + self._b.x() + self._c.x()) / 3.0_f64;
		let y_avg = (self._a.y() + self._b.y() + self._c.y()) / 3.0_f64;
		Vector2::new_f64(x_avg, y_avg)
	}
}

impl std::fmt::Display for Triangle2 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "math::Triangle2{{a:{}, b:{}, c:{}}}", self._a, self._b, self._c)
    }
}
