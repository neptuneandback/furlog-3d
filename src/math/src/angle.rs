extern crate std;
use functions;
use Angle::*;

#[derive(Copy, Clone)]
pub enum Angle {
	Degrees(f64),
	Radians(f64),
}

impl Angle {
	pub fn normalize(self) -> Angle {
		match self {
			Degrees(d) => {
				let n = functions::normalize_f64(d, 0.0, 360.0);
				Degrees(n)
			}
			Radians(r) => {
				let n = functions::normalize_f64(r, 0.0, 2.0 * ::PI);
				Radians(n)
			}
		}
	}

	//Converts an Angle to type Radians
	pub fn as_radians(self) -> Angle {
		match self {
			Degrees(d) => Radians(d.to_radians()),
			_ => self,
		}
	}

	//Converts an Angle to type Degrees
	pub fn as_degrees(self) -> Angle {
		if let Radians(r) = self {
			Degrees(r.to_degrees())
		} else {
			self
		}
	}

	pub fn as_i32(&self) -> i32 {
		match self {
			Degrees(d) => *d as i32,
			Radians(r) => *r as i32
		}
	}

	pub fn as_i64(&self) -> i64 {
		match self {
			Degrees(d) => *d as i64,
			Radians(r) => *r as i64
		}
	}

	pub fn as_f32(&self) -> f32 {
		match self {
			Degrees(d) => *d as f32,
			Radians(r) => *r as f32
		}
	}

	pub fn as_f64(&self) -> f64 {
		match self {
			Degrees(d) => *d,
			Radians(r) => *r
		}
	}
}

impl std::fmt::Display for Angle {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Degrees(d) => write!(f, "{} degrees", d),
			Radians(r) => write!(f, "{} radians", r),
		}
    }
}
