pub mod triangle2;
pub mod angle;
pub mod vector2;
pub mod functions;

pub use self::angle::Angle;
pub use self::triangle2::Triangle2;
pub use self::vector2::Vector2;

pub static PI: f64 = std::f64::consts::PI;
pub static E: f64 = std::f64::consts::E;
