Before contributing to Furlog 3D, please read the following:  
* You agree to license your contributions to Furlog 3D under [The License](https://gitlab.com/neptuneandback/furlog-3d/blob/master/LICENSE).
* Be concise.
* Think of 3 reasons why your contribution is important. If you can't think of 3, it's not worth it.
* Document everything.