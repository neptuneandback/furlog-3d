 Furlog 3D General Design Document  
August 5, 2018  
Copyright 2018 Neptune and Back, LLC  

Contributors:
  * Kayler Renslow

# Overview
Fl3d's testing framework shall be it's own independent entity (let's call it GRTF, meaning "Generic Rust Testing Framework"). The framework shall not even know the engine exists and shall be designed as if it is a general purpose testing framework.

# Core

## `Testable` Trait
The `Testable` trait is used throughout the GRTF for testing equivalence and gathering human readable information about the object being tested.

```rust
trait Testable<RHS = Self> {
  fn equals(&self, other:&RHS) -> bool;
  fn to_string(&self) -> std::string::String;
}
```

#### Testing Equivalence
```rust
//trait Testable:
fn equals(&self, other:RHS) -> bool;
```
The `equals` method is used for testing equivalence. This method returns true if and only if `self` and `other` are equal.

#### "to_string" Method
```rust
//trait Testable:
fn to_string(&self) -> std::string::String;
```
The `to_string()` method is meant to get human readable insight into the inner state of the `Testable` object. In the event of a test failure, this method may be invoked to present more contextual information.

## `TestsRunner` Struct
```rust
struct TestsRunner {
  tests: std::vec::Vec<Test>
}

impl TestsRunner {
  fn run_tests(&self) {
    // ...
  }
}
```

## `Test` Trait
The `Test` trait is a representation of a test. There can be 1 or multiple assertions within the `Test`.
```rust
trait Test {
  /// This method is run to set up the test.
  /// This method should be used to allocate necessary memory, etc,
  /// and prepare the test to run.
  /// This method will also be run before any tests are executed.
  fn init(&self);

  /// This method will actually run the tests.
  /// An Assert instance is provided to do assertions.
  fn run_test(&self, assert:&Assert);

  /// This method will run after all tests have been executed.
  fn post_test(&self);
}
```

## `Assert` Struct
The `Assert` struct is used to make assertions. Some functions can use
the internal `Reporter` instance in the `Assert` instance.

```rust
struct Assert {
  reporter: Reporter
}

impl Assert {
  /// Asserts that to Testable objects are equalivalent by using their .equals()
  /// methods. This method will not log any information, but will print to
  /// stderr in the event of an assertion fail.
  fn assert_equals<T:Testable>(&self, t1:&T, t2:&T) {
    // ...
  }

  /// This method is similar to assert_equals(), however,
  /// this method has additional functionality: a Reporter will be used.
  /// Whether the assertion fails or succeeds, the internal Reporter will be
  /// notified of the assertion result.
  fn rep_assert_equals<T:Testable, R:Reporter>(&self, t1:&T, t2:&T) {
    // ...
  }

  /// This method will force report a failure to the Reporter instance.
  fn rep_fail<R:Reporter>(&self, msg:&std::string::String) {
    // ...
  }

  /// This method will force report a success to the Reporter instance.
  fn rep_success<R:Reporter>(&self, msg:&std::string::String) {
    // ...
  }
}
```

# Assert Result Collection
Assertion result collection can be used to handle logging assertion failures
or even dynamically handling potential errors.

## `Reporter` Trait
The `Reporter` is where all assertion results flow through.

```rust
trait Reporter {
  fn log(&self, rr:&AssertReport) {
    // ...
  }
}
```

## `AssertReport` Struct
A AssertReport contains contextual information (like time) as well as information
about an assertion that failed or succeeded. This struct can also be used to store error codes or other miscellaneous information.

```rust
struct AssertReport {
  // can store time, date, a message, and additional properties about the report
}

impl AssertReport {
  /// Returns a new AssertReport instance
  fn new(/*...*/) -> AssertReport {
    return AssertReport {/*...*/};
  }
}
```
