Furlog 3D General Design Document  
July 29, 2018  
Copyright 2018 Neptune and Back, LLC

# The Vision
Furlog 3D (abbreviated as Fl3d) is an open source 3D game engine that is designed with automated testing, OS independence as well as API independence, feature poor, and developer sanity at the core of it's design. Fl3d should be able to make modern AAA games. This may not happen for a while, but that's the goal. Fl3d will be fully written in Rust.

> What do these terms mean? Why are they important?

#### Automated Testing
Both the engine and games made with the engine should have a suite of testing utilities that either don't need the full game running or doesn't need the game running at all in order to test gameplay (playtest) and the engine itself. This should be accomplished with testing _fixtures_, a Gameplay Halt Mechanism (more on this below), and testing functions that can be used during or not during gameplay.

**Why this is important:**
Automated testing is important because it will make it easier to develop Fl3d as well as games made with Fl3d. Automated testing should in theory  reduce the amount of regression testing/play testing that needs to be done by persons.

###### Gameplay Halt Mechanism
The GHM is basically an internal "break point" for the whole game and game engine. When the GHM is activated, the whole game and game engine abruptly halts. During the halt, the developer can run tests that can take a tremendous amount of time to compute without actually affecting the game or game engine. These tests should not manipulate any data, but that will not be enforced to allow the developer to test whatever they want to. After the tests have run, a function can be invoked to then start the engine and game back up like nothing ever happened.

**Why this is important:** This mechanism will allow for computationally intense testing during gameplay without actually affecting the game itself.

#### OS Independence / API Independence
Fl3d internal code should be completely isolated, except in API implementations, from OS or other 3rd party API functions and variables. Fl3d should have its own file system API, its own window API, etc.

**Why this is important:** This isolation will help port Fl3d to multiple platforms.

#### Feature Poor
Fl3d will not be a "feature rich" game engine. This does not mean it won't have a lot of features, however. _Fl3d should strive to have as few features as possible._ All features in Fl3d should solve a big or important problem. If a feature is implemented that saves only 10 seconds of developer time, it probably shouldn't exist. However, if said feature also greatly simplifies a Fl3d API and reduces a great chunk of lines of code, it's probably worth implementing.

**Why this is important:**
This methodology should help simplify Fl3d and make it easier to maintain.

#### Developer Sanity
Many computer programs are made to be fast, efficient, and solve a problem. With the increasing speed of computers, you would think that programs would be getting faster. Instead of things like basic text editors being faster, they appear to be just as slow or slower than they were years ago. Clearly, either software is getting slower or the problems are getting harder or both. whatever the case may be, there will always be one problem that can't be addressed enough: DEVELOPER SANITY. Software is not made for computers, it's made for people. Software is, at least currently, not made by computers, it is made by people.

No matter how fast the computer is or how hard the problem is, someone (or in the future: something) needs to develop it. Disregarding the developer's sanity is an easy way to make a great idea or existing codebase get a completely trashy implementation.

Every feature in Fl3d should aim to either simplify or solve a problem for someone. There should never be "only just a few hundred lines of bad code". All code should be elegant and easily readable. If speed needs to be sacrificed for readability: do it.  No one wants to make a "super fast, but ugly Ferrari"; same goes for Fl3d. Thinking in terms of readability and elegance is steroids for innovation.

**Why this is important:** Read this whole section for the argument.

# Implementation
In this section, core parts of Fl3d will be discussed. This will become more flushed out as time goes on.

## Graphics
Fl3d's graphics will be implemented with Vulkan.

## Physics
todo

## Entities
todo

## Localization
todo

## Asset Manager
todo

## Thread API
Since Fl3d will be written in Rust, and Rust's threads are mapped 1-to-1 for OS threads, Fl3d will have it's own Thread API. The GHM will be implemented by putting the whole engine on a Fl3d Thread and then simply not running said thread.

## Test API
The Fl3d testing API shall be completely standalone. Since were using Rust, it will be its own crate. Fl3d may use the testing crate to then create more specialized testing code for Fl3d, but the testing API should not even know Fl3d exists.
